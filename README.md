# osm conflation audit

This website takes a JSON output from [OSM Conflator](https://github.com/mapsme/osm_conflate)
and presents logged-in users an interface for validating each imported point, one-by-one

It records any changes and produces a file that can be later fed back to osm_conflate

## author and license

This is a [fork of a project written by Ilya Zverev](https://github.com/mapsme/cf_audit) for MAPS.ME. Published under Apache License 2.0

## setup

### python dependencies

you can use [pipenv](https://pipenv.readthedocs.io/en/latest/)

```sh
pipenv install
pipenv shell
./run.py
```

...or you can use pip (possibly need it for python 2 though)

```sh
pip install -r requirements.txt
```

### osm oauth app registration

you'll have to register your client  app on [OpenStreetMap oauth](https://www.openstreetmap.org/user/)

_make sure to select the `read their user preferences` option_

![make sure to tick the read their user preferences option](img/edit-osm-client-app-oauth.png)

### admin record in sql

using sqlite you can obtain the id number of your logged in user and add this to `config.py`

```sh
sqlite3 audit.db
```

```sql
.tables
feature  project  task     user     version

.schema user
CREATE TABLE IF NOT EXISTS "user" ("uid" INTEGER NOT NULL PRIMARY KEY, "admin" INTEGER NOT NULL, "bboxes" TEXT);

select * from user;
9465963|0|51.01375465718821,-1.8999481201171877,52.55715099278441,1.3183593750000002
```

## creating a project

### preceding partner project

the initial conflation [`.json`](https://maps.london.gov.uk/gitea/joelondon/osm-rapid-charging-stations/src/branch/master/preview.json) and [`.osm` audit](https://maps.london.gov.uk/gitea/joelondon/osm-rapid-charging-stations/src/branch/master/results.osm) files that feed into this are created run in a project [containing the `profile.py` required to output the necessary files](https://maps.london.gov.uk/gitea/joelondon/osm-rapid-charging-stations)

[some](https://lists.openstreetmap.org/pipermail/talk-gb-london/2019-March/thread.html) [help](https://lists.openstreetmap.org/pipermail/talk-gb-london/2019-April/thread.html) was given by osm community members, and the [board of the osm cic](https://osmuk.org/directors/) recommended using the osm conflation tool and audit frontend

### making an audit project

having created conflation `.json` and `.osm` files, and made yourself an admin in your client app db (see section on sqlite above), log in and you'll see the option to create/update a project

_make sure the project short name has no spaces in it as it is used to form the url_

_make sure to select the `Enable validation` option_

![make sure to tick the enable validation option](img/edit-osm-conflation-audit-project.png)

### taking it to the osm community

_the client app should be published online and made available to the osm community for the purposes of validating the data_

> You should not upload any imports to OpenStreetMap without discussing them first. In case of conflated points, you have a GeoJSON file to show, so your fellow mappers can find mistakes in tagging and estimate the geometric precision of new points. You can do even better: ask an opinion for each of the imported points. For that, you would need to install Conflation Audit web application.

![https://wiki.openstreetmap.org/wiki/OSM_Conflator#Community_Validation](img/Conflate_audit_chart.jpg)
